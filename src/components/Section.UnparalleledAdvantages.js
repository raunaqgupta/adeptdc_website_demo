import React from "react"
import styled from "styled-components"
import { media } from "../styles/media"
import { StaticQuery, graphql } from "gatsby"
import { Wrapper } from "./Layout.Wrapper"

const SectionContainer = styled.div`
  display: flex;
  flex-direction: column;
  border-top: 1px solid rgba(60, 88, 86, 0.33);
`

const FeatureContainer = styled.ul`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-column-gap: 16px;
  list-style: none;
  padding: 0;
  margin: 2em 0 0;

  ${media.largePhone`
    grid-template-columns: 1fr;
    grid-gap: 0em 0em;
  `}
`

const Title = styled.h2`
  color: #3c5856;
  font-size: 24px;
  font-weight: 700;
  margin: 48px 0 0;

  ${media.largePhone`
    font-size: 1.2em;
  `};
`

const FeatureItemContainer = styled.li`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  margin-top: 24px;
  padding: 24px 16px;
  background: white;
  border-radius: 8px;
  box-shadow: 0px 0px 8px rgba(60, 88, 86, 0.5);

  ${media.largePhone`
    padding: 1em;
  `};
`

const FeatureTitle = styled.div`
  font-size: 1em;
  color: #000;
  margin-top: 1em;
`

const FeatureIcon = styled.img`
  width: 36px;
  height: 36px;
`

export function SectionUnparalleledAdvantages() {
  return (
    <StaticQuery
      query={queryUnparalleledAdvantages}
      render={data => (
        <Wrapper>
          <SectionContainer>
            <Title>{data.queryUnparalleledAdvantages.title}</Title>
            <FeatureContainer>
              {data.queryUnparalleledAdvantages.list.map(Feature)}
            </FeatureContainer>
          </SectionContainer>
        </Wrapper>
      )}
    ></StaticQuery>
  )
}

function Feature({ title, icon }) {
  return (
    <FeatureItemContainer key={title}>
      <FeatureIcon src={icon}></FeatureIcon>
      <FeatureTitle>{title}</FeatureTitle>
    </FeatureItemContainer>
  )
}

const queryUnparalleledAdvantages = graphql`
  query {
    queryUnparalleledAdvantages: componentsYaml(
      id: { eq: "section.unparalleledAdvantages" }
    ) {
      title
      list {
        icon
        title
      }
    }
  }
`
