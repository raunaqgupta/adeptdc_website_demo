import React from "react"
import styled from "styled-components"
import { media } from "../styles/media"
import { StaticQuery, graphql } from "gatsby"
import { Wrapper } from "./Layout.Wrapper"

const SectionContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  border-top: 1px solid rgba(60, 88, 86, 0.33);
`

const FeatureContainer = styled.ul`
  display: flex;
  flex-direction: column;
  flex-grow: 2;
  list-style: none;
  padding: 0;
  margin: 2em 0;

  ${media.largePhone`
    grid-template-columns: 1fr;
    grid-gap: 0em 0em;
  `}
`

const Title = styled.h2`
  color: #3c5856;
  font-size: 24px;
  font-weight: 700;
  margin: 48px 0;

  ${media.largePhone`
    font-size: 1.2em;
  `};
`

const _Feature = styled.li`
  display: flex;
  align-items: center;
  margin-top: 24px;
  padding: 24px 48px;

  &:nth-child(even) {
    background: linear-gradient(90deg, #cbfff5 0%, #ffffff 99.07%);
    flex-direction: row;
  }

  &:nth-child(odd) {
    background: linear-gradient(270deg, #cbfff5 0%, #ffffff 100%);
    flex-direction: row-reverse;

    > div {
      text-align: left;
    }
  }

  ${media.largePhone`
    padding: 1em;
  `};
`

const FeatureTitle = styled.div`
  flex-grow: 2;
  text-align: right;
  font-size: 28px;
  font-weight: bold;
  line-height: 48px;
  color: #3c5856;
`

const FeatureIcon = styled.img`
  width: 72px;
  height: 72px;
`

export function SectionStopWastingTime() {
  return (
    <StaticQuery
      query={queryStopWastingTime}
      render={data => (
        <Wrapper>
          <SectionContainer>
            <FeatureContainer>
              <Title>{data.queryStopWastingTime.title}</Title>
              {data.queryStopWastingTime.list.map(Feature)}
            </FeatureContainer>
          </SectionContainer>
        </Wrapper>
      )}
    ></StaticQuery>
  )
}

function Feature({ title, icon }) {
  return (
    <_Feature key={title}>
      <FeatureIcon src={icon}></FeatureIcon>
      <FeatureTitle>{title}</FeatureTitle>
    </_Feature>
  )
}

const queryStopWastingTime = graphql`
  query {
    queryStopWastingTime: componentsYaml(
      id: { eq: "section.stopWastingTime" }
    ) {
      title
      list {
        icon
        title
      }
    }
  }
`
