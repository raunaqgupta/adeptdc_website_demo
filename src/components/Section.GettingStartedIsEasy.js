import React from "react"
import styled from "styled-components"
import { media } from "../styles/media"
import { StaticQuery, graphql } from "gatsby"
import { Wrapper } from "./Layout.Wrapper"
import consoleIllustration from "../../static/img/console_illustration.png"

const SectionContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  border-top: 1px solid rgba(60, 88, 86, 0.33);

  ${media.largePhone`
    flex-direction: column;
    align-items: flex-start;
  `}
`

const FeatureContainer = styled.ul`
  display: flex;
  flex-direction: column;
  flex-grow: 2;
  list-style: none;
  padding: 0;
  margin: 2em 0 0;

  ${media.largePhone`
    grid-template-columns: 1fr;
    grid-gap: 0em 0em;
  `}
`

const Title = styled.h2`
  color: #3c5856;
  font-size: 24px;
  font-weight: 700;
  margin: 0;
  margin-bottom: 28px;

  ${media.largePhone`
    font-size: 1.2em;
  `};
`

const _Feature = styled.li`
  display: flex;
  flex-direction: column;
  margin-top: 16px;
`

const FeatureTitle = styled.div`
  font-size: 24px;
  color: #3c5856;
`

const Illustration = styled.img``

export function SectionGettingStartedIsEasy() {
  return (
    <StaticQuery
      query={sectionMakeItEasy}
      render={data => (
        <Wrapper>
          <SectionContainer>
            <FeatureContainer>
              <Title>{data.sectionMakeItEasy.title}</Title>
              {data.sectionMakeItEasy.list.map(Feature)}
            </FeatureContainer>
            <Illustration src={consoleIllustration}></Illustration>
          </SectionContainer>
        </Wrapper>
      )}
    ></StaticQuery>
  )
}

function Feature({ title }) {
  return (
    <_Feature key={title}>
      <FeatureTitle>{title}</FeatureTitle>
    </_Feature>
  )
}

const sectionMakeItEasy = graphql`
  query {
    sectionMakeItEasy: componentsYaml(
      id: { eq: "section.gettingStartedIsEasy" }
    ) {
      title
      list {
        title
      }
    }
  }
`
