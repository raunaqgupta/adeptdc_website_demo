import React from "react"
import styled from "styled-components"
import { media } from "../styles/media"
import { Button, ButtonLink } from "../components/Common.Button"

const ShareContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin: 2em 0 0;
  align-items: center;

  > * {
    margin-right: 1em;
  }

  ${media.largePhone`
    width: 100%;
  `}
`

const BigButton = styled(Button)`
  font-size: 18px;
  padding: 8px;
`

export const ComponentDemoAndSignup = ({ title, shareUrl }) => {
  return (
    <ShareContainer>
      <BigButton>
        <ButtonLink href="https://app.adeptdc.com/login/signUp">
          Sign Up
        </ButtonLink>{" "}
      </BigButton>
    </ShareContainer>
  )
}
