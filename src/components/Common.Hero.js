import React, { Fragment } from "react"
import styled from "styled-components"
import { media } from "../styles/media"
import { Wrapper } from "../components/Layout.Wrapper"
import Header from "../components/Layout.Header"
import { headerLinks } from "../site"

const _Hero = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: top;
  position: relative;
  width: 100%;
  flex-wrap: ${props => props.wrap || "nowrap"};
  overflow: hidden;
`

const HeroTitle = styled.h1`
  text-shadow: 0 0 50px rgba(0, 0, 0, 0.175);
  font-size: var(--exo-font-size-h1);
  color: #3c5856;
  margin: 0;
  margin-top: 48px;
  font-weight: 700;
  width: ${props => props.width || "75%"};
  text-align: ${props => props.align || "left"};

  ${media.largePhone`
    width: 100%;
		font-size: 2.5em;
    padding-top: 1em;
    padding-left: 0;
    padding-right: 0;
  `};
`

const HeroSubTitle = styled.h2`
  margin: 2em 0 1em;
  font-size: 1.5em;
  font-weight: 400;
  line-height: 1.5em;
  width: 66%;
  color: #3c5856;
  text-align: ${props => props.align || "left"};

  ${media.largePhone`
    width: 100%;
		font-size: 1em;
  `};
`

const HeroWrapper = styled(Wrapper)`
  height: 500px;

  ${media.largePhone`
    height: 650px;
  `}
`

export function Hero({ title, subTitle, children, width }) {
  return (
    <Fragment>
      <_Hero>
        <Header pages={headerLinks} />
        <HeroWrapper>
          <HeroTitle width={width}>{title}</HeroTitle>
          {subTitle && <HeroSubTitle>{subTitle}</HeroSubTitle>}
          {children}
        </HeroWrapper>
      </_Hero>
    </Fragment>
  )
}
