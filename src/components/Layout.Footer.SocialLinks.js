import React from "react"
import styled from "styled-components"
import { media } from "../styles/media"

const _SocialLinks = styled.div`
  ${media.largePhone`
    font-size: 1.5em;
    line-height: 3em;
  `};
`

const Social = styled.a`
  color: #fff;
  transition: all 0.2s;
  font-size: 1.5rem;

  &:nth-child(n + 2) {
    margin-left: 10px;
  }

  &:hover {
    opacity: 1;
  }

  ${media.largePhone`
    font-size: 1.4rem;
  `};
`

export default function SocialLinks() {
  return (
    <_SocialLinks>
      <Social href="https://twitter.com/_adeptdc" target="_blank">
        <i className="fab fa-twitter" />
      </Social>
      <Social href="https://www.facebook.com/adeptdc/" target="_blank">
        <i className="fab fa-facebook" />
      </Social>
      <Social href="https://www.linkedin.com/company/adeptdc" target="_blank">
        <i className="fab fa-linkedin" />
      </Social>
    </_SocialLinks>
  )
}
