import styled from 'styled-components'

const Commons = `
  display: block;
`

export const Spacing = styled.div`
  ${Commons} height: 2em;
`
