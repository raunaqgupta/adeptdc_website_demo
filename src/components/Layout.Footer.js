import React from "react"
import Link from "gatsby-link"
import styled from "styled-components"
import { Wrapper } from "./Layout.Wrapper"
import { media } from "../styles/media"
import { footerLinks } from "../site"
import SocialLinks from "../components/Layout.Footer.SocialLinks"

const _Footer = styled.footer`
  display: flex;
  justify-content: center;
  position: relative;
  min-height: 300px;
  overflow: hidden;
  background-color: #3c5856;
`

const FooterContent = styled.div`
  padding-top: 1em;
  margin: 0 auto;
  width: 100%;
  max-width: ${props =>
    props.theme && props.theme.sizes && props.theme.sizes.maxWidth};
  display: flex;
  flex-direction: column;
  z-index: 1338;

  ${media.largePhone`
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
  `};
`

const PageLists = styled.div`
  list-style-type: none;
  width: 100%;
  padding: 0 0 2em;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: flex-start;

  ${media.largePhone`
    flex-direction: column;
    align-items: center;
  `};

  ul {
    padding-left: 0;
  }

  li {
    text-align: left;
    list-style: none;

    ${media.largePhone`
      line-height: 2;
      font-size: 1.25em;
      text-align: center;
    `};
  }

  a {
    text-decoration: none;
    color: #fff;
    font-weight: 400;
    transition: all 0.2s;
    display: inline-block;
    padding: 0;
    font-size: 110%;

    ${media.largePhone`
      font-size: 90%;
    `};
  }
`

const RowZero = styled.div``

const RowOne = styled.div`
  display: flex;
  flex-direction: column;
  line-height: 1.5;
  position: relative;
  align-items: flex-start;

  ${media.largePhone`
    flex-direction: row;
  `};
`

const RowTwo = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: baseline;
  padding: 2em 0 0;
  border-top: 1px solid #E5FBF9;

  ${media.largePhone`
    align-items: center;
    flex-direction: column;
  `};
`

const Copyright = styled.small`
  color: #93bab7;
  font-size: 0.9rem;
`

const FooterH4 = styled.h4`
  color: #93bab7;
  font-size: 1em;
  font-weight: 400;
  opacity: 0.75;
  text-align: left;
  margin: 0;

  ${media.largePhone`
    text-align: center;
    margin-bottom: 0.25em;
  `};
`

const ListSection = styled.div`
  display: flex;
  flex-direction: row;

  &:not(:last-child) {
    margin-bottom: 1em;
  }

  ${media.largePhone`
    flex-direction: column;
    
    &:not(:last-child) {
      margin-bottom: 3em;
    }
  `};
`

const ListSectionLinks = styled.ul`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin: 0;

  li {
    margin-left: 1em;
    font-size: 1em;
    color: #3c5856;
  }
`

function ListItem(props) {
  return <li>{props.children}</li>
}

function ListLinks(props) {
  const listItems = props.data.map(section => (
    <ListSection key={section.title}>
      <FooterH4>{section.title}</FooterH4>
      <ListSectionLinks>
        {section.data.map(({ id, name, to, external }) => (
          <ListItem key={id}>
            {external && external === true ? (
              <a href={to}>{name}</a>
            ) : (
              <Link to={to}>{name}</Link>
            )}
          </ListItem>
        ))}
      </ListSectionLinks>
    </ListSection>
  ))

  return listItems
}

export function Footer({ children }) {
  return (
    <_Footer>
      <Wrapper>
        <FooterContent>
          <RowZero></RowZero>
          <RowOne>
            <PageLists>
              <ListLinks data={footerLinks} />
            </PageLists>
          </RowOne>
          <RowTwo>
            <SocialLinks />
            <Copyright>©2020 AdeptDC, Inc</Copyright>
          </RowTwo>
        </FooterContent>
      </Wrapper>
    </_Footer>
  )
}
