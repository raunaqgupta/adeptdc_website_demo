import React from 'react'
import styled from 'styled-components'
import { media } from '../styles/media'
import MarkdownContent from '../components/Common.MarkdownContent'

export const BlogPostContainer = styled.ul`
  display: flex;
  list-style: none;
  padding: 0;
  margin: 0;
  flex-direction: row;
  justify-content: space-around;
  align-items: flex-start;
  flex-wrap: wrap;
`

const BlogPostCardLink = styled.a`
    width: 100%;
    text-decoration: none;
`

const BlogPostCard = styled.li`
  box-sizing: border-box;
  display: flex;
  flex-direction: row;
  align-items: top;
  width: 100%;
  background-color: white;
  margin-bottom: 1em;
  box-shadow: 0px 0px 8px rgba(60, 88, 86, 0.5);
  padding: 0;

  ${media.largePhone`
    flex-direction: column;
    margin-bottom: 1em;
    align-items: flex-start;
  `};
`

const Content = styled.div`
  margin: 2em;
`

const Name = styled.h2`
  font-size: 1.5em;
  font-weight: bold;
  margin: 0 0 5px;

  ${media.largePhone`
    text-align: left;
    margin-bottom: 1em;
  `};
`

const ImageContainer = styled.div``

const Image = styled.img`
  width: 400px;
  height: 100%;
  object-fit: cover;

  ${media.largePhone`
    width: 100%;
  `};
`

export function BlogPost({ title, link, image, description }) {
  return (
    <BlogPostCardLink href={link} >
      <BlogPostCard key={title}>
        <ImageContainer>
          <Image src={image} />
        </ImageContainer>
        <Content>
          <Name>{title}</Name>
          <MarkdownContent html={description} />
        </Content>
      </BlogPostCard>
    </BlogPostCardLink>
  )
}
