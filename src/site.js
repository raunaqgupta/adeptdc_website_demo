export const theme = {
  colors: {
    base: "#EF542D",
    secondary: "#302DEF",
    tertiary: "#f3f3f3",
    highlight: "#5b8bf7"
  },
  sizes: {
    giant: 1170,
    desktop: 920,
    tablet: 700,
    largePhone: 500,
    phone: 376,
    maxWidth: "1170px",
    maxWidthCentered: "650px"
  }
}

export const sizes = theme.sizes

export const headerLinks = [
  {
    id: "home",
    name: "Home",
    to: "/"
  },
  {
    id: "team",
    name: "Team",
    to: "/team"
  },

  {
    id: "blog",
    name: "Blog",
    to: "/blog"
  },

  {
    id: "docs",
    name: "Docs",
    to: "https://docs.adeptdc.com",
    external: true
  },
]

export const footerLinks = [
  {
    title: "Links",
    data: [
      {
        id: "home",
        name: "Home",
        to: "/"
      },
      {
        id: "team",
        name: "Team",
        to: "/team"
      },
      {
        id: "blog",
        name: "Blog",
        to: "/blog"
      },
      {
        id: "docs",
        name: "Docs",
        to: "https://docs.adeptdc.com",
        external: true
      },
    ]
  },
  {
    title: "Contact Us",
    data: [
      {
        id: "contactus",
        name: "AdeptDC Inc. 995 Market St, San Francisco, CA 94103",
        to: "https://bit.ly/3ktebf7",
        external: true
      }
    ]
  },
  {
    title: "Email",
    data: [
      {
        id: "email",
        name: "info@adeptdc.com",
        to: "mailto:info@adeptdc.com",
        external: true
      }
    ]
  },
  {
    title: "Call Us",
    data: [
      {
        id: "callus",
        name: "+1-833-233-7832",
        to: "tel:1-833-233-7832",
        external: true
      }
    ]
  }
]
