import React from 'react'
import Layout from '../components/layout'
import { graphql } from 'gatsby'
import { TitleAndMetaTags } from '../components/Helpers.TitleAndMetaTags'
import { Section } from '../components/Layout.Wrapper'

import { PageHeader } from '../components/Component.PageHeader'
import { Footer } from '../components/Layout.Footer'
import { TeamMemberContainer, TeamMember } from '../components/Team.TeamMember'

export default function TeamPage({ data }) {
  const { allPagesYaml } = data
  const pageData = allPagesYaml.edges[0].node

  return (
    <Layout>
      <TitleAndMetaTags title={pageData.title} pathname="team" />
      <PageHeader title={pageData.title}></PageHeader>
      <Section>
        <TeamMemberContainer>
          {data.team.nodes.map((edge) => {
            const { html, frontmatter } = edge
            const { name, title, image, linkedin } = frontmatter

            return (
              <TeamMember
                key={name}
                name={name}
                role={title}
                image={image}
                linkedin={linkedin}
                bio={html}
              />
            )
          })}
        </TeamMemberContainer>
      </Section>
      <Footer />
    </Layout>
  )
}

export const pageQuery = graphql`
  query teamQuery {
    team: allMarkdownRemark(filter: {fields: {collection: {eq: "team"}}}, sort: {fields: frontmatter___rank, order: ASC}) {
      nodes {
        frontmatter {
          name
          title
          image
          linkedin
        }
        html
      }
    }

    allPagesYaml(filter: { id: { eq: "team" } }) {
      edges {
        node {
          title
        }
      }
    }
  }
`