import React from "react"
import styled from "styled-components"
import Layout from "../components/layout"
import PropTypes from "prop-types"
import { graphql } from "gatsby"
import { TitleAndMetaTags } from "../components/Helpers.TitleAndMetaTags"
import { Hero } from "../components/Common.Hero"
import { Footer } from "../components/Layout.Footer"
import { SectionGettingStartedIsEasy } from "../components/Section.GettingStartedIsEasy"
import { SectionStopWastingTime } from "../components/Section.StopWastingTime"
import { SectionUnparalleledAdvantages } from "../components/Section.UnparalleledAdvantages"
import { ComponentDemoAndSignup } from "../components/Component.DemoAndSignup"
import heroIllustration from "../../static/img/homepage_illustration.png"

const Illustration = styled.img`
  position: absolute;
  bottom: 0;
  right: 0;
  z-index: -1;
`

export default function IndexPage({ data }) {
  const { allPagesYaml } = data
  const pageData = allPagesYaml.edges[0].node

  return (
    <Layout>
      <TitleAndMetaTags
        title={pageData.title}
        description={pageData.subtitle}
      />
      <Hero title={pageData.title} subTitle={pageData.subtitle}>
        <ComponentDemoAndSignup></ComponentDemoAndSignup>
        <Illustration src={heroIllustration}></Illustration>
      </Hero>
      <SectionUnparalleledAdvantages></SectionUnparalleledAdvantages>
      <SectionGettingStartedIsEasy></SectionGettingStartedIsEasy>
      <SectionStopWastingTime></SectionStopWastingTime>
      <Footer />
    </Layout>
  )
}

export const pageQuery = graphql`
  query homeQuery {
    allPagesYaml(filter: { id: { eq: "index" } }) {
      edges {
        node {
          title
          subtitle
        }
      }
    }
  }
`

IndexPage.propTypes = {
  data: PropTypes.object
}
