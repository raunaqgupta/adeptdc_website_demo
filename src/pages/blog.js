import React from 'react'
import Layout from '../components/layout'
import { graphql } from 'gatsby'
import { TitleAndMetaTags } from '../components/Helpers.TitleAndMetaTags'
import { Section } from '../components/Layout.Wrapper'

import { PageHeader } from '../components/Component.PageHeader'
import { Footer } from '../components/Layout.Footer'
import { BlogPostContainer, BlogPost } from '../components/Component.BlogPost'

export default function BlogPage({ data }) {
  const { allPagesYaml } = data
  const pageData = allPagesYaml.edges[0].node

  return (
    <Layout>
      <TitleAndMetaTags title={pageData.title} pathname="blog" />
      <PageHeader title={pageData.title}></PageHeader>
      <Section>
        <BlogPostContainer>
          {data.blog.nodes.map((edge) => {
            const { html, frontmatter } = edge
            const { title, link, image } = frontmatter

            return (
              <BlogPost
                title={title}
                link={link}
                image={image}
                description={html}
              />
            )
          })}
        </BlogPostContainer>
      </Section>
      <Footer />
    </Layout>
  )
}

export const pageQuery = graphql`
  query blogQuery {
    blog: allMarkdownRemark(filter: {fields: {collection: {eq: "blog"}}}) {
      nodes {
        frontmatter {
          title
          link
          image
        }
        html
      }
    }

    allPagesYaml(filter: { id: { eq: "blog" } }) {
      edges {
        node {
          title
        }
      }
    }
  }
`