module.exports = {
  siteMetadata: {
    title: "AdeptDC",
    siteUrl: "https://adeptdc.com"
  },
  plugins: [
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `AdeptDC`,
        short_name: `AdeptDC`,
        start_url: `/`,
        theme_color: `#ffffff`,
        background_color: `#ffffff`,
        display: `standalone`,
        lang: `en`,
        icon: `src/images/logo.png`,
        icons: [
          {
            src: "/android-chrome-192x192.png",
            sizes: "192x192",
            type: "image/png"
          },
          {
            src: "/android-chrome-512x512.png",
            sizes: "512x512",
            type: "image/png"
          }
        ]
      }
    },
    {
      resolve: "gatsby-plugin-html-attributes",
      options: {
        lang: "en"
      }
    },
    {
      resolve: "gatsby-plugin-robots-txt",
      options: {
        host: "https://adeptdc.com",
        sitemap: "https://adeptdc.com/sitemap.xml",
        policy: [
          {
            userAgent: "*",
            allow: "/",
            disallow: "/admin"
          }
        ]
      }
    },
    {
      resolve: "gatsby-plugin-sitemap",
      options: {
        exclude: ["/faq/*", "/thanks", "/team/*"]
      }
    },
    "gatsby-plugin-react-helmet",
    {
      resolve: "gatsby-plugin-nprogress",
      options: {
        color: "#ffffff",
        showSpinner: false
      }
    },
    "gatsby-plugin-styled-components",
    "gatsby-transformer-json",

    "gatsby-transformer-yaml",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "pages",
        path: `${__dirname}/content/pages/`
      }
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "components",
        path: `${__dirname}/content/components/`
      }
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "team",
        path: `${__dirname}/content/team/`
      }
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "blog",
        path: `${__dirname}/content/blog/`
      }
    },
    "gatsby-plugin-netlify",
    {
      resolve: `gatsby-plugin-google-tagmanager`,
      options: {
        id: process.env.GOOGLE_TAG_MANAGER_ID,
        includeInDevelopment: true
      }
    },
    {
      resolve: "gatsby-plugin-web-font-loader",
      options: {
        google: {
          families: ["Nunito Sans:400,600,700"]
        }
      }
    },
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          `gatsby-remark-copy-linked-files`,
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 650,
              showCaptions: true,
              markdownCaptions: true
            }
          }
        ]
      }
    },
    "gatsby-plugin-catch-links",
    "gatsby-plugin-lodash"
  ]
}
