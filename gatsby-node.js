const slug = require("slug")
const path = require("path")

exports.onCreateNode = ({ node, getNode, actions }) => {
  console.log(node);
  if (node.internal.type === "MarkdownRemark") {
    const { createNodeField } = actions
    const parent = getNode(node.parent)
    if (parent.name && parent.sourceInstanceName) {
      createNodeField({
        node,
        name: "slug",
        value: slug(parent.name)
      })

      createNodeField({
        node,
        name: "collection",
        value: parent.name === "noop" ? "noop" : parent.sourceInstanceName
      })
    }
  }
}

exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
  if (stage === "build-html") {
    actions.setWebpackConfig({
      module: {
        rules: [
          {
            test: /details-element-polyfill/,
            use: loaders.null()
          }
        ]
      }
    })
  }
}
