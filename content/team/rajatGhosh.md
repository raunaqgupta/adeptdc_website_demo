---
rank: 0
name: Rajat Ghosh
title: CEO
linkedin: rajat11
image: /img/team/rajatGhosh.jpg
---

Rajat loves to build algorithms to solve critical system optimization challenges like cascading failures, multi-scale data center capacity planning. He has a PhD from Georgia Tech and won several competitive federal grants including NSF SBIR. As the CEO, he focuses on product innovation.