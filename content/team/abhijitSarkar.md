---
rank: 1
name: Abhijit Sarkar
title: CTO
linkedin: abhijit-sarkar-a0434b55
image: /img/team/abhijitSarkar.png
---

Abhijit is a driven technologist who believes in the power of distributed systems. He has a decade-long experience in software engineering for big tech companies like Microsoft, Facebook, and Amazon. As the CTO, he focuses on engineering. 