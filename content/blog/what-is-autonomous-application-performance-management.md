---
title: What is Autonomous Application Performance Management (AAPM)?
link: https://medium.com/@admin_21959/autonomous-application-performance-management-apm-2c4a8a5486be
image: /img/blog/press-image-aapm.jpg
---

Autonomous application performance management (AAPM) is a new paradigm of application performance management (APM).