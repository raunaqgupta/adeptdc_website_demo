---
title: AdeptDC wants to use machine learning to prevent data center outages
link: https://www.datacenterknowledge.com/machine-learning/adeptdc-wants-use-machine-learning-prevent-data-center-outages
image: /img/blog/press-image-dcd.jpg
---

As Google has demonstrated, applying machine learning to understand heat patterns and fine-tune the data center cooling system for maximum efficiency is a sound data center use case for machine...